package com.example.githubuser.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.githubuser.databinding.CollasibleIssueViewBinding
import com.example.githubuser.model.UserIssueDataModel
import java.util.*

class CollapsibleUserIssueListRecyclerViewAdapter(
    var list: ArrayList<String>,
    var linkedHashMap: LinkedHashMap<String, ArrayList<UserIssueDataModel>>
) :
    RecyclerView.Adapter<CollapsibleUserIssueListRecyclerViewAdapter.ModuleViewHolder>() {

    fun updateData(
        mlist: ArrayList<String>,
        linkedHashMap: LinkedHashMap<String, ArrayList<UserIssueDataModel>>
    ) {
        this.list = mlist
        this.linkedHashMap = linkedHashMap
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModuleViewHolder {
        return ModuleViewHolder(
            CollasibleIssueViewBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }


    override fun onBindViewHolder(holder: ModuleViewHolder, position: Int) {
        holder.bind(list.get(position), linkedHashMap)
    }

    class ModuleViewHolder(val inflater: CollasibleIssueViewBinding) :
        RecyclerView.ViewHolder(inflater.root) {
        fun bind(
            moduleName: String,
            linkedHashMap: LinkedHashMap<String, ArrayList<UserIssueDataModel>>
        ) {
            inflater.collapsibleCardIssue.setData(
                moduleName,
                linkedHashMap.get(moduleName)
            )

        }
    }


}