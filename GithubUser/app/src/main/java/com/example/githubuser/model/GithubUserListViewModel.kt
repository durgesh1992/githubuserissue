package com.example.githubuser.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.githubuser.api.Loading
import com.example.githubuser.api.Result
import com.example.githubuser.db.GithubUserDataDao
import com.example.githubuser.use_case.GithubUseCase
import com.example.githubuser.utils.ConnectionExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

class GithubUserListViewModel @Inject constructor(val githubUseCase: GithubUseCase) : ViewModel() {
    val githubUserDataMutableLiveData: MutableLiveData<Result<GitHubUserResponse>> =
        MutableLiveData();
    val githubUserIssueDataMutableLiveData: MutableLiveData<Result<GithubIssueListModel>> =
        MutableLiveData();
    val githubUserIssueCommentDataMutableLiveData: MutableLiveData<Result<List<GithubIssueCommentModel>>> =
        MutableLiveData();
    @Inject
    lateinit var connectionHandler: ConnectionExceptionHandler
    @Inject
    lateinit var githubDao: GithubUserDataDao


    fun getGithubUserList(map: Map<String, String>) {
        viewModelScope.launch(connectionHandler.captureException()) {
            githubUserDataMutableLiveData.value = Loading(true);
            val result: Result<GitHubUserResponse> =
                githubUseCase.getGithubUserList(map)
            githubUserDataMutableLiveData.value = result
        }

    }

    fun getOfflineList(): List<GithubIssueListModelItem> {
        val list = githubDao.getGithubUserIssueList()
        return list
    }

    fun saveList(mList: List<GithubIssueListModelItem>) {
        mList.forEach { item ->
            githubDao.insert(item)
        }
    }

    fun getGithubUserIssueList() {
        viewModelScope.launch(connectionHandler.captureException()) {
            githubUserIssueDataMutableLiveData.value = Loading(true);
            val result: Result<GithubIssueListModel> =
                githubUseCase.getGithubUserIssueList()
            githubUserIssueDataMutableLiveData.value = result
        }

    }


}
