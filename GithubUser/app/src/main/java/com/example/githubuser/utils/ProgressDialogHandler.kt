package com.example.githubuser.utils

import android.annotation.TargetApi
import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.githubuser.R




//Global progress dialog
class ProgressDialogHandler {
    private var mAlertDialog: AlertDialog? = null
    val isShowing: Boolean
        get() = if (mAlertDialog != null) {
            mAlertDialog!!.isShowing
        } else {
            false
        }


    //Show dialog
    @TargetApi(17)
    fun showProgressDialog(context: Context) {

        val alertBuilder = AlertDialog.Builder(context)

        alertBuilder.setCancelable(false)

        val view = LayoutInflater.from(context).inflate(R.layout.progress_dialog, null)
        val imageView = view.findViewById<ImageView>(R.id.gif_spinner) as ImageView

        Glide.with(context).asGif().load(R.drawable.loader).into(imageView);
        alertBuilder.setView(view)
        mAlertDialog = alertBuilder.create()
        //mAlertDialog!!.window!!.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        mAlertDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mAlertDialog!!.show()

    }/**/

    //Dismiss dialog
    fun dismissDialog() {
        //checking for null
        if (mAlertDialog != null && mAlertDialog!!.isShowing) {
            mAlertDialog!!.dismiss()
        }
    }
}
