package com.example.githubuser.adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.githubuser.databinding.GithubUserItemLayoutBinding
import com.example.githubuser.model.Item
import com.example.githubuser.model.UserIssueDataModel

class GithubRecyclerViewAdapter(var itemDataList: List<UserIssueDataModel>, val itemListener: ItemListener) :
    RecyclerView.Adapter<GithubRecyclerViewAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            GithubUserItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
            , itemListener
        )
    }
     fun updateList(list:List<UserIssueDataModel>) {
        this.itemDataList=list
         notifyDataSetChanged()
    }
    override fun getItemCount(): Int {
        return itemDataList.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(itemDataList.get(position), position)
    }

    inner class ItemViewHolder(
        val inflater: GithubUserItemLayoutBinding,
        val itemListener: ItemListener
    ) :
        RecyclerView.ViewHolder(inflater.root) {
        fun bind(item: UserIssueDataModel, position: Int) {
            val view = inflater.root;
            inflater.tvUsername.text = "${item.userName}"
            inflater.tvCommentBody.text = "${item.commentBody}"
//            Glide.with(view.context).load(item.avatar_url).into(inflater.ivUserAvatar)
            inflater.clView.setOnClickListener {
                val mPosition = adapterPosition
                itemListener.onItemClicked(it, mPosition)
            }
        }
    }

    interface ItemListener {
        fun onItemClicked(view: View, position: Int)
    }

}