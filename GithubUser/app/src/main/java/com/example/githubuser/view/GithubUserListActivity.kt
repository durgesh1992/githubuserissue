package com.example.githubuser.view

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.githubuser.R
import com.example.githubuser.adapter.CollapsibleUserIssueListRecyclerViewAdapter
import com.example.githubuser.api.Failure
import com.example.githubuser.api.Success
import com.example.githubuser.databinding.ActivityMainBinding
import com.example.githubuser.eventbus.MessageEvent
import com.example.githubuser.model.GithubIssueListModel
import com.example.githubuser.model.GithubUserListViewModel
import com.example.githubuser.model.UserIssueDataModel
import com.example.githubuser.utils.InternetConnectionHelper
import com.example.githubuser.utils.ProgressDialogHandler
import com.google.gson.Gson
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*
import javax.inject.Inject
import kotlin.collections.LinkedHashMap

class GithubUserListActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    val githubUserListViewModel: GithubUserListViewModel by viewModels() {
        viewModelFactory
    }

    var activity: Activity? = null
    var TAG = GithubUserListActivity::class.java.simpleName

    @Inject
    lateinit var progressDialogHandler: ProgressDialogHandler


    var collapsibleUserIssueListRecyclerViewAdapter: CollapsibleUserIssueListRecyclerViewAdapter? =
        null
    var mUserList = ArrayList<String>()
    var userListMap = LinkedHashMap<String, ArrayList<UserIssueDataModel>>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)
        observeGithubUserIssueList()
        initializeAdapter()
    }


    private fun initializeAdapter() {

        collapsibleUserIssueListRecyclerViewAdapter =
            CollapsibleUserIssueListRecyclerViewAdapter(mUserList, userListMap)
        binding.recyclerView.adapter = collapsibleUserIssueListRecyclerViewAdapter
    }

    override fun onResume() {
        super.onResume()
        if (InternetConnectionHelper.isConnected(this)) {
            callGithubUserIssueListAPI()
        } else {

            if (githubUserListViewModel.getOfflineList() != null) {
                val userMap = LinkedHashMap<String, ArrayList<UserIssueDataModel>>()
                val mUserList = ArrayList<String>()
                for (item in githubUserListViewModel.getOfflineList() .indices) {
                    val githubIssueListModel =githubUserListViewModel.getOfflineList()[item]
                    val userKey = githubIssueListModel.user.login
                    if (userMap != null) {
                        if (userMap.containsKey(userKey)) {
                            val mOldList = userMap.get(userKey)
                            val userIssueDataModel = UserIssueDataModel()
                            userIssueDataModel.commemtUrl =
                                githubIssueListModel.comments_url
                            userIssueDataModel.commentBody = githubIssueListModel.body
                            userIssueDataModel.commentTitle = githubIssueListModel.title
                            userIssueDataModel.issueNumber=githubIssueListModel.number.toString()
                            userIssueDataModel.userName = userKey
                            mOldList!!.add(userIssueDataModel)
                            userMap.put(userKey, mOldList)
                        } else {
                            val mList = ArrayList<UserIssueDataModel>()
                            val userIssueDataModel = UserIssueDataModel()
                            userIssueDataModel.commemtUrl =
                                githubIssueListModel.comments_url
                            userIssueDataModel.commentBody = githubIssueListModel.body
                            userIssueDataModel.commentTitle = githubIssueListModel.title
                            userIssueDataModel.issueNumber=githubIssueListModel.number.toString()
                            userIssueDataModel.userName = userKey
                            mList.add(userIssueDataModel)
                            userMap.put(
                                userKey,
                                mList
                            )
                            mUserList.add(userKey)
                        }
                    } else {
                        if (userMap == null) {
                            val mList = ArrayList<UserIssueDataModel>()
                            val userIssueDataModel = UserIssueDataModel()
                            userIssueDataModel.commemtUrl =
                                githubIssueListModel.comments_url
                            userIssueDataModel.commentBody = githubIssueListModel.body
                            userIssueDataModel.commentTitle = githubIssueListModel.title
                            userIssueDataModel.userName = userKey
                            userIssueDataModel.issueNumber=githubIssueListModel.number.toString()
                            mList.add(userIssueDataModel)
                            userMap.put(
                                userKey,
                                mList
                            )
                            mUserList.add(userKey)
                        }
                    }

                }

                collapsibleUserIssueListRecyclerViewAdapter.let {
                    mUserList.let {
                        collapsibleUserIssueListRecyclerViewAdapter!!.updateData(
                            mUserList,
                            userMap
                        )
                    }

                }
            } else {
                Toast.makeText(activity, "Check Your Internet", Toast.LENGTH_LONG)
                    .show()
            }
        }


    }


    private fun callGithubUserIssueListAPI() {

        progressDialogHandler.showProgressDialog(activity!!)
        githubUserListViewModel!!.getGithubUserIssueList()
    }

    private fun observeGithubUserIssueList() {
        githubUserListViewModel.githubUserIssueDataMutableLiveData.observe(
            this@GithubUserListActivity,
            Observer {
                when (it) {
                    is Success<GithubIssueListModel> -> {
                        progressDialogHandler.dismissDialog()
                        Log.d(TAG, "Response : " + it.data)
                        val userMap = LinkedHashMap<String, ArrayList<UserIssueDataModel>>()
                        val mUserList = ArrayList<String>()
                        for (item in it.data.indices) {
                            val githubIssueListModel = it.data[item]
                            val userKey = githubIssueListModel.user.login
                            if (userMap != null) {
                                if (userMap.containsKey(userKey)) {
                                    val mOldList = userMap.get(userKey)
                                    val userIssueDataModel = UserIssueDataModel()
                                    userIssueDataModel.commemtUrl =
                                        githubIssueListModel.comments_url
                                    userIssueDataModel.commentBody = githubIssueListModel.body
                                    userIssueDataModel.commentTitle = githubIssueListModel.title
                                    userIssueDataModel.issueNumber=githubIssueListModel.number.toString()
                                    userIssueDataModel.userName = userKey
                                    mOldList!!.add(userIssueDataModel)
                                    userMap.put(userKey, mOldList)
                                } else {
                                    val mList = ArrayList<UserIssueDataModel>()
                                    val userIssueDataModel = UserIssueDataModel()
                                    userIssueDataModel.commemtUrl =
                                        githubIssueListModel.comments_url
                                    userIssueDataModel.commentBody = githubIssueListModel.body
                                    userIssueDataModel.commentTitle = githubIssueListModel.title
                                    userIssueDataModel.issueNumber=githubIssueListModel.number.toString()
                                    userIssueDataModel.userName = userKey
                                    mList.add(userIssueDataModel)
                                    userMap.put(
                                        userKey,
                                        mList
                                    )
                                    mUserList.add(userKey)
                                }
                            } else {
                                if (userMap == null) {
                                    val mList = ArrayList<UserIssueDataModel>()
                                    val userIssueDataModel = UserIssueDataModel()
                                    userIssueDataModel.commemtUrl =
                                        githubIssueListModel.comments_url
                                    userIssueDataModel.commentBody = githubIssueListModel.body
                                    userIssueDataModel.commentTitle = githubIssueListModel.title
                                    userIssueDataModel.userName = userKey
                                    userIssueDataModel.issueNumber=githubIssueListModel.number.toString()
                                    mList.add(userIssueDataModel)
                                    userMap.put(
                                        userKey,
                                        mList
                                    )
                                    mUserList.add(userKey)
                                }
                            }

                        }
                        githubUserListViewModel.saveList( it.data)

                        collapsibleUserIssueListRecyclerViewAdapter.let {
                            mUserList.let {
                                collapsibleUserIssueListRecyclerViewAdapter!!.updateData(
                                    mUserList,
                                    userMap
                                )
                            }

                        }

                    }
                    is Failure -> {
                        progressDialogHandler.dismissDialog()
                    }
                }

            })
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {


        if (event != null && event.commentUrl != null) {

            val intent = Intent(this@GithubUserListActivity, GithubCommentListActivity::class.java)
            val messageEventString = Gson().toJson(event, MessageEvent::class.java)
            intent.putExtra("CommentBody", messageEventString)
            startActivity(intent)
        }


    }


}
