package com.example.githubuser.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.githubuser.model.GithubIssueListModelItem
import com.example.githubuser.model.Item

@Dao
interface GithubUserDataDao
{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(githubData: GithubIssueListModelItem): Long

    @Query("SELECT * FROM GithubIssueListModelItem")
    abstract fun getGithubUserIssueList(): List<GithubIssueListModelItem>
}