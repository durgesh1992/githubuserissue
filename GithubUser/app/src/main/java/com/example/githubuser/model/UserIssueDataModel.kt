package com.example.githubuser.model

data class UserIssueDataModel(
    var commemtUrl: String = "",
    var commentBody: String = "",
    var commentTitle: String = "",
    var userName: String = "",
    var issueNumber: String = ""
)