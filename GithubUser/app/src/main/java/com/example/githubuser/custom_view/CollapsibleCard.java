/*
 * Copyright (c) 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.example.githubuser.custom_view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;

import com.example.githubuser.R;
import com.example.githubuser.adapter.IssueContentRecyclerViewAdapter;
import com.example.githubuser.eventbus.MessageEvent;
import com.example.githubuser.model.UserIssueDataModel;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.M;

public class CollapsibleCard extends FrameLayout {

    private boolean mExpanded = false;
    private TextView mCardTitle;
    private ImageView mExpandIcon;
    private View mTitleContainer;
    private RecyclerView mRecyclerView;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CollapsibleCard(Context context) {
        this(context, null);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CollapsibleCard(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public CollapsibleCard(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.CollapsibleCard, 0, 0);
        final String cardTitle = arr.getString(R.styleable.CollapsibleCard_cardTitle);
        final String cardDescription = arr.getString(R.styleable.CollapsibleCard_cardDescription);
        arr.recycle();
        final View root = LayoutInflater.from(context)
                .inflate(R.layout.collapsible_card_issue_list, this, true);
        mCardTitle = root.findViewById(R.id.card_title);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.issue_recyclerview);
        mTitleContainer = (RelativeLayout) root.findViewById(R.id.title_container);
        mExpandIcon = (ImageView) root.findViewById(R.id.expand_icon);
        if (SDK_INT < M) {
            mExpandIcon.setImageTintList(
                    AppCompatResources.getColorStateList(context, R.color.white));
        }
        final Transition toggle = TransitionInflater.from(getContext())
                .inflateTransition(R.transition.info_card_toggle);
        final OnClickListener expandClick = new OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                mExpanded = !mExpanded;
                toggle.setDuration(mExpanded ? 300L : 200L);
                TransitionManager.beginDelayedTransition((ViewGroup) root.getParent(), toggle);

                mRecyclerView.setVisibility(mExpanded ? VISIBLE : GONE);

                mExpandIcon.setRotation(mExpanded ? 360f : 0f);
                // activated used to tint controls when expanded
                mExpandIcon.setActivated(mExpanded);
                mCardTitle.setActivated(mExpanded);
                if (mExpanded) {
                    mRecyclerView.setVisibility(VISIBLE);


                } else {
                    mRecyclerView.setVisibility(GONE);


                }
            }
        };


        mTitleContainer.setOnClickListener(expandClick);
    }

    public void setData(String userName,
                        ArrayList<UserIssueDataModel> issueInfoArrayList) {

        mCardTitle.setText(userName);

        IssueContentRecyclerViewAdapter issueContentRecyclerViewAdapter =
                new IssueContentRecyclerViewAdapter(issueInfoArrayList, new IssueContentRecyclerViewAdapter.ItemListener() {
                    @Override
                    public void onItemClicked(@NotNull View view, int position) {
                        UserIssueDataModel userIssueDataModel = issueInfoArrayList.get(position);
                        MessageEvent event = new MessageEvent();
                        event.commentUrl = userIssueDataModel.getCommemtUrl();
                        event.issueNumber = userIssueDataModel.getIssueNumber();
                        event.userName = userIssueDataModel.getUserName();
                        event.issueTitle = userIssueDataModel.getCommentTitle();
                        EventBus.getDefault().post(event);
                    }
                });
        mRecyclerView.setAdapter(issueContentRecyclerViewAdapter);


    }


}
