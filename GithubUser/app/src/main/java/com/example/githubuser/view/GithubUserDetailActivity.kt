package com.example.githubuser.view

import android.app.Activity
import android.app.DownloadManager
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.View
import android.webkit.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.githubuser.R
import com.example.githubuser.databinding.ActivityDetailBinding
import com.example.githubuser.utils.ProgressDialogHandler
import javax.inject.Inject

class GithubUserDetailActivity : AppCompatActivity() {
    private val REQUEST_CODE_ASK_PERMISSIONS = 101
    var urlToLoad = ""
    lateinit var binding: ActivityDetailBinding
    var activity: Activity? = null
    var TAG = GithubUserDetailActivity::class.java.simpleName

    @Inject
    lateinit var progressDialogHandler: ProgressDialogHandler


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_detail)
        activity = this
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_detail)
        intent?.let {

            if (it.hasExtra("HTML_URL"))
                urlToLoad = it.getStringExtra("HTML_URL")!!
            loadWebViewData()
        }
    }

    private fun loadWebViewData() {
        var url = urlToLoad

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.githubWebView.clearCache(true)
            binding.githubWebView.getSettings()
                .setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }

        binding.githubWebView.getSettings().setJavaScriptEnabled(true);
        binding.githubWebView.getSettings().allowUniversalAccessFromFileURLs = true
        binding.githubWebView.getSettings().allowFileAccessFromFileURLs = true
        binding.githubWebView.getSettings().setDomStorageEnabled(true);
        binding.githubWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        binding.githubWebView.getSettings().setBuiltInZoomControls(true);
        Log.d(TAG, " url loading " + url)
        binding.githubWebView.loadUrl(url)
        binding.progressbar.visibility=View.VISIBLE
        binding.githubWebView.setWebViewClient(getWebViewClient());
        binding.githubWebView.setWebChromeClient(object : WebChromeClient() {
            override fun onConsoleMessage(consoleMessage: ConsoleMessage): Boolean {
                Log.d(TAG, "WebView : " + consoleMessage.message())
                return true
            }


        }

        )
        binding.githubWebView.setDownloadListener(DownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
            val request = DownloadManager.Request(
                Uri.parse(url)
            )
            val fileName = URLUtil.guessFileName(url, contentDisposition, mimetype);
            request.allowScanningByMediaScanner()
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_DOWNLOADS + "GithubFolder" + "/",
                fileName
            )
            val dm: DownloadManager =
                getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            dm.enqueue(request)
            Handler().postDelayed(
                {
                    binding.progressbar.visibility = View.GONE
                }, 1500
            )


        }

        )
    }


    private fun getWebViewClient(): WebViewClient? {
        return object : WebViewClient() {

            override fun shouldOverrideUrlLoading(
                view: WebView,
                request: WebResourceRequest
            ): Boolean {
                binding.progressbar.visibility = View.VISIBLE
                view.loadUrl(request.url.toString()/*, getCustomHeaders()*/)
                return true
            }

            @SuppressWarnings("deprecation")
            @Override
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                binding.progressbar.visibility = View.VISIBLE
                view.loadUrl(url/*, getCustomHeaders()*/)
                return true
            }


            override fun onReceivedSslError(
                view: WebView?,
                handler: SslErrorHandler?,
                error: SslError?
            ) {
                // ignore ssl error
                if (handler != null) {
                    handler.proceed()
                } else {
                    super.onReceivedSslError(view, null, error)
                }
            }


            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                binding.progressbar.visibility=View.GONE

            }


        }
    }


    override fun onPause() {
        super.onPause()
        if (binding.githubWebView != null) {
            binding.githubWebView.stopLoading()
        }
    }

    override fun onStop() {
        super.onStop()
        if (binding.githubWebView != null) {
            binding.githubWebView.stopLoading()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_ASK_PERMISSIONS -> if (grantResults.size > 0) {
                val smsPermission = grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED

                if (smsPermission) {
                    Toast.makeText(
                        activity, "Permission Granted",
                        Toast.LENGTH_LONG
                    ).show()
                    loadWebViewData()
                    binding.progressbar.visibility = View.VISIBLE
                } else {
                    Toast.makeText(activity, "Permission Denied", Toast.LENGTH_LONG)
                        .show()
                }
            }
        }
    }
}
