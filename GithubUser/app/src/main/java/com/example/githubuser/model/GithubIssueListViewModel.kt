package com.example.githubuser.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.githubuser.api.Loading
import com.example.githubuser.api.Result
import com.example.githubuser.db.GithubUserDataDao
import com.example.githubuser.use_case.GithubUseCase
import com.example.githubuser.utils.ConnectionExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

class GithubIssueListViewModel @Inject constructor(val githubUseCase: GithubUseCase) : ViewModel() {

    val githubUserIssueCommentDataMutableLiveData: MutableLiveData<Result<GithubIssueCommentModel>> =
        MutableLiveData();
    @Inject
    lateinit var connectionHandler: ConnectionExceptionHandler
    @Inject
    lateinit var githubDao: GithubUserDataDao



    fun getOfflineList(): List<Item> {
        val list = githubDao.getGithubData()
        return list
    }

    fun saveList(mList: List<Item>) {
        mList.forEach { item ->
            githubDao.insert(item)
        }
    }


    fun getGithubIssueCommentList(commentId: String) {
        viewModelScope.launch(connectionHandler.captureException()) {
            githubUserIssueCommentDataMutableLiveData.value = Loading(true);
            val result: Result<GithubIssueCommentModel> =
                githubUseCase.getGithubUserIssueCommentDetail(commentId)
            githubUserIssueCommentDataMutableLiveData.value = result
        }

    }
}
