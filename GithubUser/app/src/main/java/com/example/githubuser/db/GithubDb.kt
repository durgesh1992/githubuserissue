package com.example.githubuser.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.githubuser.model.GithubIssueListModelItem
import com.example.githubuser.model.Item

@Database(entities = [GithubIssueListModelItem::class],version = 2, exportSchema = false)

abstract class GitHubDb  :RoomDatabase()
{
    abstract val githubUserDataDao : GithubUserDataDao
}
