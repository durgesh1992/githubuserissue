package com.example.githubuser.utils

import android.content.Context
import android.util.Log
import android.widget.Toast
import kotlinx.coroutines.CoroutineExceptionHandler
import javax.inject.Inject

class ConnectionExceptionHandler  @Inject constructor(val context: Context) {

    private val TAG = ConnectionExceptionHandler::class.java.simpleName
    fun captureException(): CoroutineExceptionHandler {
        return CoroutineExceptionHandler { coroutineContext, throwable ->
            Log.e(TAG, "Caught $throwable")
            Toast.makeText(
                context,
                "Please check your internet connection",
                Toast.LENGTH_LONG
            ).show()
//            snackBarHandler.raiseSimpleSnackBar("Please check your internet")
        }
    }
}