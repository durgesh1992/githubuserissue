package com.example.githubuser.model

data class GitHubUserResponse(
    val incomplete_results: Boolean,
    val items: List<Item>,
    val total_count: Int
)