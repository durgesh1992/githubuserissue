package com.example.githubuser.view

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.githubuser.R
import com.example.githubuser.adapter.GithubRecyclerViewAdapter
import com.example.githubuser.api.Failure
import com.example.githubuser.api.Success
import com.example.githubuser.databinding.ActivityMainBinding
import com.example.githubuser.eventbus.MessageEvent
import com.example.githubuser.model.*
import com.example.githubuser.utils.InternetConnectionHelper
import com.example.githubuser.utils.ProgressDialogHandler
import com.google.gson.Gson
import java.util.*
import javax.inject.Inject

class GithubCommentListActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    val githubIssueListViewModel: GithubIssueListViewModel by viewModels() {
        viewModelFactory
    }

    var activity: Activity? = null
    var TAG = GithubCommentListActivity::class.java.simpleName

    @Inject
    lateinit var progressDialogHandler: ProgressDialogHandler


    var mIssueList = ArrayList<UserIssueDataModel>()
    var githubRecyclerViewAdapter: GithubRecyclerViewAdapter? = null
    var messageEvent: MessageEvent? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)
        intent?.let {
            if (it.hasExtra("CommentBody")) {
                val messageEventString = it.getStringExtra("CommentBody")
                messageEvent = Gson().fromJson(messageEventString, MessageEvent::class.java)
            }
        }
        observeGithubUserIssueList()
        initializeAdapter()
    }


    private fun initializeAdapter() {

        githubRecyclerViewAdapter =
            GithubRecyclerViewAdapter(mIssueList, object : GithubRecyclerViewAdapter.ItemListener {
                override fun onItemClicked(view: View, position: Int) {

                }
            })
        binding.recyclerView.adapter = githubRecyclerViewAdapter
    }

    override fun onResume() {
        super.onResume()
        if (InternetConnectionHelper.isConnected(this)) {
            callGithubIssueCommentListAPI()
        } else {

            if (githubIssueListViewModel.getOfflineList() != null) {

            } else {
                Toast.makeText(activity, "Check Your Internet", Toast.LENGTH_LONG)
                    .show()
            }
        }


    }


    private fun callGithubIssueCommentListAPI() {

        progressDialogHandler.showProgressDialog(activity!!)
        githubIssueListViewModel!!.getGithubIssueCommentList(messageEvent!!.issueNumber)
    }

    private fun observeGithubUserIssueList() {
        githubIssueListViewModel.githubUserIssueCommentDataMutableLiveData.observe(
            this@GithubCommentListActivity,
            Observer {
                when (it) {
                    is Success<GithubIssueCommentModel> -> {
                        progressDialogHandler.dismissDialog()
                        Log.d(TAG, "Response : " + it.data)
                        if(it.data.size>0) {
                            mIssueList = ArrayList<UserIssueDataModel>()
                            for (item in it.data.indices) {
                                val githubIssueCommentModel = it.data[item]
                                val userIssueDataModel = UserIssueDataModel()
                                userIssueDataModel.commemtUrl =
                                    messageEvent!!.commentUrl
                                userIssueDataModel.commentBody = githubIssueCommentModel.body
                                userIssueDataModel.commentTitle = messageEvent!!.issueTitle
                                userIssueDataModel.issueNumber = messageEvent!!.issueNumber
                                userIssueDataModel.userName = messageEvent!!.userName
                                mIssueList.add(userIssueDataModel)
                            }

                            githubRecyclerViewAdapter?.let {
                                githubRecyclerViewAdapter!!.updateList(mIssueList)
                            }
                            binding.recyclerView.visibility=View.VISIBLE
                            binding.tvNoComments.visibility=View.GONE
                        }else
                        {
                            binding.recyclerView.visibility=View.GONE
                            binding.tvNoComments.visibility=View.VISIBLE
                        }

                    }
                    is Failure -> {
                        progressDialogHandler.dismissDialog()
                    }
                }

            })
    }

}
