package com.example.githubuser.adapter



import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.githubuser.databinding.GithubIssueItemLayoutBinding
import com.example.githubuser.model.UserIssueDataModel

class IssueContentRecyclerViewAdapter(val issueList: List<UserIssueDataModel>?, val itemListener: ItemListener) :
    RecyclerView.Adapter<IssueContentRecyclerViewAdapter.CourseViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CourseViewHolder {
        return CourseViewHolder(
            GithubIssueItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
            , itemListener
        )
    }

    override fun getItemCount(): Int {
        return issueList!!.size
    }

    override fun onBindViewHolder(holder: CourseViewHolder, position: Int) {
        holder.bind(issueList!!.get(position),position)
    }

    inner class CourseViewHolder(
        val inflater: GithubIssueItemLayoutBinding,
        val itemListener: ItemListener
    ) :
        RecyclerView.ViewHolder(inflater.root) {


        fun bind(dataItem: UserIssueDataModel,position: Int) {
            inflater.tvIssueTitle.setText(dataItem.commentTitle)
            inflater.tvIssueBody.setText(dataItem.commentBody)
            inflater.root.setOnClickListener {
                itemListener.onItemClicked(inflater.root,position)
            }



        }
    }





    interface ItemListener {
        fun onItemClicked(view: View, position: Int)
    }

}