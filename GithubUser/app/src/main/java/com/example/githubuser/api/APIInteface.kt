package com.example.githubuser.api


import com.example.githubuser.model.GitHubUserResponse
import com.example.githubuser.model.GithubIssueCommentModel
import com.example.githubuser.model.GithubIssueListModel

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface APIInteface {
    @GET("/search/users")
    fun getGitHubUserList(@Header("Accept") token: String, @QueryMap map: Map<String, String>): Deferred<Response<GitHubUserResponse>>

    @GET("issues")
    fun getIssueList(): Deferred<Response<GithubIssueListModel>>
    @GET("issues/{issueId}/comments")
    fun getCommentDetailOfIssue( @Path("issueId") issueId:String): Deferred<Response<GithubIssueCommentModel>>
}

